import {
  AppRegistry,
} from 'react-native';

import { StackNavigator } from 'react-navigation';

import StationsScreen from './components/StationsScreen.js';
import PlayerScreen from './components/PlayerScreen.js';

const RadioSydney = StackNavigator({
  Stations: { screen: StationsScreen },
  Player: { screen: PlayerScreen },
}, { headerMode: 'none' });

AppRegistry.registerComponent('RadioSydney', () => RadioSydney);
