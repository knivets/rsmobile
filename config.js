const baseDomain = 'https://radio.sydney';

export default {
  baseUrl: baseDomain,
  streamsUrl: baseDomain + '/mobile/streams.json',
  metaUrl: baseDomain + '/mobile/meta.php',
}
