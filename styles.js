import {
  StyleSheet,
} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#000',
    justifyContent: 'space-between',
    flexDirection: 'column',
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  link: {
    fontSize: 24,
    color: '#fff',
    fontWeight: 'bold',
  },
  linkActive: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#4EFD2F'
  },
  emailIcon: {
    alignSelf: 'center',
    marginRight: 20
  },
  qualityContainer: {
    flexDirection: 'row',
    height: 29,
    alignSelf: 'center',
    marginTop: 20
  },
  qualityItem: {
    marginLeft: 10,
    marginRight: 10,
    height: 29
  },
  controlsContainer: {
    flexDirection: 'row',
    //justifyContent: 'space-between',
    alignSelf: 'center',
    marginHorizontal: 20
  },
  lineImg: {
    width: 160,
    height: 60,
    marginHorizontal: 15
  },
  stationsScreen: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#000',
    flexDirection: 'column',
  },
  stationImage: {
    width: 220,
    height: 100
  },
  playerScreen: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#000',
    flexDirection: 'column',
  },
  footerContainer: {
    flex: 1,
    alignItems: 'center',
    marginHorizontal: 10,
    marginTop: 10,
    marginBottom: 30
  },
  metaContainer: {
    flex: 3,
    alignItems: 'center',
    marginHorizontal: 10,
    justifyContent: 'center',
  },
  songContainer: {
    alignItems: 'center',
    marginHorizontal: 10,
    marginTop: 10,
  },
  songTitle: {
    color: 'white',
    textAlign: 'center',
    fontSize: 20,
  },
  songArtist: {
    color: 'white',
    fontSize: 22,
    textAlign: 'center',
    fontWeight: 'bold'
  },
  songAlbum: {
    color: 'white',
    fontSize: 20,
    fontStyle: 'italic',
    textAlign: 'center',
  },
  songYear: {
    color: 'white',
    textAlign: 'center',
    fontSize: 20,
  },
  logo: {
    width: 260,
    height: 100,
    marginBottom: 10,
    flex: 1
  }
});
