import React, { Component } from 'react';
import {
  Text,
  TouchableHighlight,
  Image,
  View
} from 'react-native';

import styles from '../styles';

export default class StationsScreen extends Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.stationsScreen}>
        <Image source={require('../images/logo.png')} style={{marginTop:10,marginBottom:40, width: 300, height: 130}} />
        <View>
          <View>
            <TouchableHighlight onPress={() => navigate('Player', {station: 'red'})}>
              <Image source={require('../images/red.png')} style={styles.stationImage} />
            </TouchableHighlight>
          </View>
          <View>
            <TouchableHighlight onPress={() => navigate('Player', {station: 'white'})}>
              <Image source={require('../images/white.png')} style={styles.stationImage} />
            </TouchableHighlight>
          </View>
          <View>
            <TouchableHighlight onPress={() => navigate('Player', {station: 'blue'})}>
              <Image source={require('../images/blue.png')} style={styles.stationImage} />
            </TouchableHighlight>
          </View>
        </View>
      </View>
    )
  }
}
