import React, { Component } from 'react';
import { ReactNativeAudioStreaming } from 'react-native-audio-streaming';
import {
  Text,
  DeviceEventEmitter,
  View
} from 'react-native';

import config from '../config';

export default class Audio extends Component {
  constructor(props) {
    super(props);
    this.state = {
      streams: null,
    };
  }
  getCurrentStreamUrl() {
    return this.state.streams[this.props.station][this.props.quality];
  }
  componentDidMount() {
    var self = this
    fetch(config.streamsUrl, {
      headers: {
        'Cache-Control': 'no-cache'
      }
    })
    .then((res) => { return res.json() })
    .then((res) => {
      self.setState({streams: res.streams})
      ReactNativeAudioStreaming.play(self.getCurrentStreamUrl(), {showIniOSMediaCenter: true, showInAndroidNotifications: true});
    });
  }
	componentWillUnmount() {
		ReactNativeAudioStreaming.stop();
	}
  componentDidUpdate(prevProps, prevState) {
    let urlChanged = !(prevProps.station === this.props.station && prevProps.quality === this.props.quality);
    if(urlChanged || (!urlChanged && !prevProps.play && this.props.play))
    {
      ReactNativeAudioStreaming.play(this.getCurrentStreamUrl(), {showIniOSMediaCenter: true, showInAndroidNotifications: true});
    }
    else if(!this.props.play)
    {
      ReactNativeAudioStreaming.pause();
    }
  }
  render() {
    if(this.state.streams)
    {
      return (
        <View></View>
      )
    }
    else
    {
      return null
    }
  }
}
