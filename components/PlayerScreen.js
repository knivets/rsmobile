import React, { Component } from 'react';
import {
  Text,
  TouchableHighlight,
  Image,
  View,
	Alert
} from 'react-native';

import {default as Mailer} from 'react-native-mail';

import styles from '../styles';
import config from '../config';
import Audio from './Audio.js';

export default class PlayerScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      metaCache: {
        blue: {
          cover: false,
          song: false
        },
        white: {
          cover: false,
          song: false
        },
        red: {
          cover: false,
          song: false
        }
      },
			station: 'white',
      quality: '128',
      play: true,
      song: {
        artist: '',
        title: '',
        album: '',
        year: ''
      },
    };
    this.state.playPauseImages = {
      play: {
        white: require('../images/play-white.png'),
        blue: require('../images/play-blue.png'),
        red: require('../images/play-red.png'),
      },
      pause: {
        white: require('../images/pause-white.png'),
        blue: require('../images/pause-blue.png'),
        red: require('../images/pause-red.png'),
      }
    }
    this.togglePlay = this.togglePlay.bind(this);
		this.state.station = this.props.navigation.state.params.station;
		this.state.cover = config.baseUrl + '/meta/' + this.state.station + '/covers/art-00.jpg?' + Math.random();
  }
  isUpToDate(cb) {
    var self = this;
    fetch(config.metaUrl, {
      headers: {
        'Cache-Control': 'no-cache'
      }
    }).then((res) => { return res.json() })
      .then((res) => {
        var station = self.state.station;
        var metaCache = self.state.metaCache;
        var data = res[station];
        var songStr = data['song']['artist'] + data['song']['artist'];
        if(metaCache[station]['cover'] !== data['cover']['time'])
        {
          metaCache[station]['cover'] = data['cover']['time']
          cb({cover: data['cover']['url']});
        }
        if(metaCache[station]['song'] !== songStr)
        {
          metaCache[station]['song'] = songStr
          cb({song: data['song']});
        }
      });
  }
  changeQuality(quality) {
    this.setState({
      quality: String(quality),
      play: true
    })
  }
  togglePlay() {
    var playbackStatus = this.state.play;
    if(playbackStatus)
      this.setState({play: false})
    else
      this.setState({play: true})
  }
  componentDidMount() {
		var self = this;
		this.timerID = setInterval(function(){
			self.isUpToDate(function(res){
        if(res.cover)
        {
          self.setState({cover: res.cover + '?' + Math.random()})
        }
        if(res.song)
        {
          self.setState({song: res.song})
        }
			});
		}, 1000);
  }
  toggleMail() {
		Mailer.mail({
			subject: 'Mobile App Support',
			recipients: ['maxturner@radio.sydney'],
			body: '',
		}, (error, event) => {
				if(error) {
					Alert.alert('Error', 'Could not send mail. Please send a mail to maxturner@radio.sydney');
				}
		});
  }
	componentWillUnmount() {
    clearInterval(this.timerID);
  }
	currQuality(q) {
		return this.state.quality === String(q)
	}
  trimSongMeta(s, limit) {
    if(!limit)
    {
      limit = 10;
    }
    if(s.length > limit)
    {
      return s.slice(0, limit) + '...';
    }
    return s;
  }
  render() {
    var imgs = this.state.playPauseImages;
    const navigation = this.props.navigation;
    const navigate = navigation.navigate;
    return (
      <View style={styles.playerScreen}>
        <Image source={require('../images/logo.png')} style={styles.logo} />
        <View style={styles.metaContainer}>
          <Image source={{uri: this.state.cover}} style={{width: 200, height: 200}} />
          <View style={styles.songContainer}>
            <View>
              <Text style={styles.songTitle}>{this.trimSongMeta(this.state.song.title, 20)}</Text>
            </View>
            <View>
              <Text style={styles.songArtist}>{this.trimSongMeta(this.state.song.artist, 18)}</Text>
            </View>
            <View>
              <Text style={styles.songAlbum}>{this.trimSongMeta(this.state.song.album, 20)}</Text>
            </View>
            <View>
              <Text style={styles.songYear}>{this.state.song.year}</Text>
            </View>
          </View>
        </View>
        <Audio station={this.state.station} quality={this.state.quality} play={this.state.play}/>
        <View style={styles.footerContainer}>
          <View style={styles.controlsContainer}>
            <TouchableHighlight onPress={this.togglePlay} style={{alignSelf: 'center'}}>
              <Image source={imgs[(this.state.play ? 'pause' : 'play')][this.state.station]} style={{width: 64, height: 64}} />
            </TouchableHighlight>
            <TouchableHighlight onPress={() => navigation.goBack()} style={{alignSelf: 'center'}}>
              <Image source={require('../images/line.png')} style={styles.lineImg} />
            </TouchableHighlight>
            <TouchableHighlight onPress={() => this.toggleMail()} style={{alignSelf: 'center'}}>
              <Image source={require('../images/mail.png')} style={{width: 50, height: 50}} />
            </TouchableHighlight>
          </View>
          <View style={styles.qualityContainer}>
            <TouchableHighlight onPress={() => this.changeQuality(320)} style={styles.qualityItem}>
              <Text style={this.currQuality(320) ? styles.linkActive : styles.link}>320</Text>
            </TouchableHighlight>
            <TouchableHighlight onPress={() => this.changeQuality(128)} style={styles.qualityItem}>
              <Text style={this.currQuality(128) ? styles.linkActive : styles.link}>128</Text>
            </TouchableHighlight>
            <TouchableHighlight onPress={() => this.changeQuality(64)} style={styles.qualityItem}>
              <Text style={this.currQuality(64) ? styles.linkActive : styles.link}>64</Text>
            </TouchableHighlight>
            <TouchableHighlight onPress={() => this.changeQuality(32)} style={styles.qualityItem}>
              <Text style={this.currQuality(32) ? styles.linkActive : styles.link}>32</Text>
            </TouchableHighlight>
          </View>
        </View>
      </View>
    );
  }
}
